//
//  SideMenuViewController.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var tableViewSideMenu: UITableView!
    var timer: Timer?
    var arrMenuItems = ["Dashboard","Deposit","Participation","Asset Portfolio","Brokerage Plans", "Personal Settings", "Help Center", "Logout"]
    
    
    // MARK: - View Controller Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = Date.getDateString()
        self.labelTime.text = "\(Date.getTimeString()) EST"
        

        
        vwHeader.frame = CGRect.init(x: 0, y: 0, width: 375 * scaleFactorX, height: 170 * scaleFactorX)
        tableViewSideMenu.tableHeaderView = vwHeader
        
        tableViewSideMenu.delegate = self
        tableViewSideMenu.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTimeOfDate), userInfo: nil, repeats: true)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    deinit
    {
        print("SideMenuViewController Deinit")
    }

    
    // MARK: - Custome Methods

    @objc func getTimeOfDate() {
        self.labelTime.text = "\(Date.getTimeString()) EST"

    }

    
    
}

extension SideMenuViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell") as! SideMenuTableCell
        
        cell.labelText.text = arrMenuItems[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 7:
            UserDefaults.deleteUserInformation()
            APPDELEGATE.screenRedirect()
            break
        default:
            break
        }
        print(indexPath.row)
    }
    
    
}
