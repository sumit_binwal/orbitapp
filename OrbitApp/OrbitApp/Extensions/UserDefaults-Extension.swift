//
//  UserDefaults-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 24/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation

public extension UserDefaults {
    enum UserDefaultsKeys: String {
        case userInformation = "userInformation"
        case languageInfo = "language"
        case isSocialLogin = "socialLogin"
        case userID = "userID"
    }
    
    enum UserInfoKeys: String {
        case ssid = "ssid"
        case _id = "id"
        case mobile = "mobile"
        case countryCode = "countryCode"
        case name = "name"
        case email = "email"
        case picture = "picture"
        case gender = "gender"
        case dob = "dob"
        case preferences = "preferences"
        case album = "album"
        case isPro = "isPro"
        case notificationUnreadCount = "notificationUnreadCount"
        case chatUnreadCount = "chatUnreadCount"
        case notificationStatus = "notificationStatus"
        case notificationNormalStatus = "all"
        case notificationFollowRequestStatus = "followRequest"
        case notificationTravelChatStatus = "travelChat"
        case notificationBudEChatStatus = "budeEChat"
        case notificationFlightAlertsStatus = "flightAlerts"
    }
}

extension UserDefaults {
    
    static func saveLanguageInformation(userInfo: String) {
        let userDefaults = UserDefaults (suiteName: "language")!
        let data = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        userDefaults.setValue(data, forKey: UserDefaultsKeys.languageInfo.rawValue)
    }
    
    static func getLanguageInformation() -> String? {
        let userDefaults = UserDefaults (suiteName: "language")!
        guard let data = userDefaults.value(forKey: UserDefaultsKeys.languageInfo.rawValue) as? Data else {
            return nil
        }
        
        guard let userInfo = NSKeyedUnarchiver.unarchiveObject(with: data) as? String else {
            return nil
        }
        
        return userInfo
    }
    
    static func saveUserInformation(userInfo: [String:Any]) {
        let userDefaults = UserDefaults (suiteName: "user")!
        let data = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        userDefaults.setValue(data, forKey: UserDefaultsKeys.userInformation.rawValue)
    }
    
    static func deleteUserInformation() {
        let userDefaults = UserDefaults (suiteName: "user")!
        userDefaults.removeObject(forKey: UserDefaultsKeys.userInformation.rawValue)
        userDefaults.synchronize()
    }
    
    static func getUserInformation() -> [String:Any]? {
        
        let userDefaults = UserDefaults (suiteName: "user")!
        
        guard let data = userDefaults.value(forKey: UserDefaultsKeys.userInformation.rawValue) as? Data else {
            return nil
        }
        
        guard let userInfo = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String:Any] else {
            return nil
        }
        
        return userInfo
    }
    
    
    
    static func getSocialLogin() -> Bool? {
        
        let userDefaults = UserDefaults (suiteName: "socialLogin")!
        
        guard let socilaLogin = userDefaults.value(forKey: UserDefaultsKeys.isSocialLogin.rawValue) as? Bool else {
            return nil
        }
        
        return socilaLogin
    }
    
    static var userSSID: String? {
        guard let info = getUserInformation() else {return nil}
        guard let ssid = info[UserInfoKeys.ssid.rawValue] as? String else {return nil}
        return ssid
    }
    
    static var userEmail: String? {
        guard let info = getUserInformation() else {return nil}
        guard let email = info[UserInfoKeys.email.rawValue] as? String else {return nil}
        return email
    }
    
    static var userName: String? {
        guard let info = getUserInformation() else {return nil}
        guard let name = info[UserInfoKeys.name.rawValue] as? String else {return nil}
        return name
    }
    
    static var userCountryCode: String? {
        guard let info = getUserInformation() else {return nil}
        guard let countryCode = info[UserInfoKeys.countryCode.rawValue] as? String else {return nil}
        return countryCode
    }
    
    static var mobileNumber: String? {
        guard let info = getUserInformation() else {return nil}
        guard let _id = info[UserInfoKeys.mobile.rawValue] as? String else {return nil}
        return _id
    }
    
    static var profilePic: String? {
        guard let info = getUserInformation() else {return nil}
        guard let profilePic = info[UserInfoKeys.picture.rawValue] as? String else {return nil}
        let pic = String.MyApp.AWS_BASE_URL + String.AWSFolder.UsersDir + profilePic
        return pic
    }
    
    static var userID: String? {
        guard let info = getUserInformation() else {return nil}
        guard let _id = info[UserInfoKeys._id.rawValue] as? String else {return nil}
        
        return _id
    }
    
    static var userGender: String? {
        guard let info = getUserInformation() else {return nil}
        guard let gender = info[UserInfoKeys.gender.rawValue] as? String else {return nil}
        return gender
    }
    
    static func saveSocialLogin(socilaLogin: Bool) {
        let userDefaults = UserDefaults (suiteName: "socialLogin")!
        userDefaults.set(socilaLogin, forKey: UserDefaultsKeys.isSocialLogin.rawValue)
    }
    
    static var userDOB: String? {
        guard let info = getUserInformation() else {return nil}
        guard let dob = info[UserInfoKeys.dob.rawValue] as? String else {return nil}
        let dateStr = UtilityClass.getDateStringFromStr(givenDateStr: dob, andCurrentDateFormat: "dd-MM-yyyy", andResultingDateFormat: "dd MMMM yyyy")
        return dateStr
    }
    
    static var notificationNormalStatus: Bool? {
        get{
            guard let info = getUserInformation() else {return nil}
            guard let dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool] else {return nil}
            
            return dict[UserInfoKeys.notificationNormalStatus.rawValue]
        }
        set{
            guard var info = getUserInformation() else {return }
            if var dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool]{
                dict[UserInfoKeys.notificationNormalStatus.rawValue] = newValue
                info[UserInfoKeys.notificationStatus.rawValue] = dict
                saveUserInformation(userInfo: info)
            }
        }
    }
    
    static var notificationFollowRequestStatus: Bool? {
        get{
            guard let info = getUserInformation() else {return nil}
            guard let dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool] else {return nil}
            
            return dict[UserInfoKeys.notificationFollowRequestStatus.rawValue]
        }
        set{
            guard var info = getUserInformation() else {return }
            if var dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool]{
                dict[UserInfoKeys.notificationFollowRequestStatus.rawValue] = newValue
                info[UserInfoKeys.notificationStatus.rawValue] = dict
                saveUserInformation(userInfo: info)
            }
        }
    }
    
    static var notificationTravelChatStatus: Bool? {
        get{
            guard let info = getUserInformation() else {return nil}
            guard let dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool] else {return nil}
            
            return dict[UserInfoKeys.notificationTravelChatStatus.rawValue]
        }
        set{
            guard var info = getUserInformation() else {return }
            if var dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool]{
                dict[UserInfoKeys.notificationTravelChatStatus.rawValue] = newValue
                info[UserInfoKeys.notificationStatus.rawValue] = dict
                saveUserInformation(userInfo: info)
            }
        }
    }
    
    static var notificationBudEChatStatus: Bool? {
        get{
            guard let info = getUserInformation() else {return nil}
            guard let dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool] else {return nil}
            
            return dict[UserInfoKeys.notificationBudEChatStatus.rawValue]
        }
        set{
            guard var info = getUserInformation() else {return }
            if var dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool]{
                dict[UserInfoKeys.notificationBudEChatStatus.rawValue] = newValue
                info[UserInfoKeys.notificationStatus.rawValue] = dict
                saveUserInformation(userInfo: info)
            }
        }
    }
    
    static var notificationFlightAlertsStatus: Bool? {
        get{
            guard let info = getUserInformation() else {return nil}
            guard let dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool] else {return nil}
            
            return dict[UserInfoKeys.notificationFlightAlertsStatus.rawValue]
        }
        set{
            guard var info = getUserInformation() else {return }
            if var dict = info[UserInfoKeys.notificationStatus.rawValue] as? [String: Bool]{
                dict[UserInfoKeys.notificationFlightAlertsStatus.rawValue] = newValue
                info[UserInfoKeys.notificationStatus.rawValue] = dict
                saveUserInformation(userInfo: info)
            }
        }
    }
    
    static var notificationUnreadCount: Int {
        get {
            let userDefaults = UserDefaults (suiteName: "user")!
            let count = userDefaults.integer(forKey: UserInfoKeys.notificationUnreadCount.rawValue)
            return count
        }
        set {
            let userDefaults = UserDefaults (suiteName: "user")!
            userDefaults.set(newValue, forKey: UserInfoKeys.notificationUnreadCount.rawValue)
        }
    }
    
    static var chatUnreadCount: Int {
        get {
            let userDefaults = UserDefaults (suiteName: "user")!
            let count = userDefaults.integer(forKey: UserInfoKeys.chatUnreadCount.rawValue)
            return count
        }
        set {
            let userDefaults = UserDefaults (suiteName: "user")!
            userDefaults.set(newValue, forKey: UserInfoKeys.chatUnreadCount.rawValue)
        }
    }
    
    static var isProUser: Bool {
        get {
            guard let info = getUserInformation() else {return false}
            guard let preferences = info[UserInfoKeys.isPro.rawValue] as? Bool else {return false}
            return preferences
        }
        set {
            guard var info = getUserInformation() else {return}
            info[UserInfoKeys.isPro.rawValue] = newValue
            UserDefaults.saveUserInformation(userInfo: info)
        }
    }
    
    static var userPreferences: [String] {
        get {
            guard let info = getUserInformation() else {return []}
            guard let preferences = info[UserInfoKeys.preferences.rawValue] as? [String] else {return []}
            return preferences
        }
        set {
            guard var info = getUserInformation() else {return}
            info[UserInfoKeys.preferences.rawValue] = newValue
            UserDefaults.saveUserInformation(userInfo: info)
        }
    }
    
    static var userAlbum: [[String: Any]]? {
        get {
            guard let info = getUserInformation() else {return nil}
            guard let album = info[UserInfoKeys.album.rawValue] as? [[String:Any]] else {return nil}
            return album
        }
        set {
            guard var info = getUserInformation() else {return}
            info[UserInfoKeys.album.rawValue] = newValue
            UserDefaults.saveUserInformation(userInfo: info)
        }
    }
    
    
    static func addMediaNameToAlbum(_ mediaInfo: [String: Any], with type: String) {
        guard var album = UserDefaults.userAlbum else {
            return
        }
        
        if album.count > 0 {
            album.insert(mediaInfo, at: 0)
        }
        else {
            album.append(mediaInfo)
        }
        
        UserDefaults.userAlbum = album
    }
}
