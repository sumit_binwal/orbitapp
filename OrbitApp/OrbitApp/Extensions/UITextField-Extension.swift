//
//  UITextField-Extension.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 10/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

extension UITextField {

    //IBINspectable Method To Change PlaceHolder Color
        @IBInspectable var placeHolderColor: UIColor? {
            get {
                return self.placeHolderColor
            }
            set {
                self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
            }
        }

}
