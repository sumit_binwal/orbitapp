//
//  UIViewController-Extension.swift
//  WaltzinUser
//
//  Created by Sumit Sharma on 10/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    struct BarButtonTypes: OptionSet {
        let rawValue: Int
        
        static let searchButton = BarButtonTypes(rawValue: 1 << 0)
        static let addBudEChatButton = BarButtonTypes(rawValue: 1 << 1)
    }
    
    enum BarButtonPosition {
        case left
        case right
    }
    
    //    private var popoverMenuVisiblity = false
    //    var isPopOverMenuVisible: Bool {
    //        get {
    //            return popoverMenuVisiblity
    //        }
    //        set {
    //            popoverMenuVisiblity = newValue
    //        }
    //    }
    
    var backBarButtonForReset: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "backIcon"), style: .plain, target: self, action: #selector(onBackButtonActionForReset))
        self.navigationItem.leftBarButtonItem = button
        return button
    }
    
    var backBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "back_image"), style: .plain, target: self, action: #selector(onBackButtonAction))
        self.navigationItem.leftBarButtonItem = button
        return button
    }
    
    var menuBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: UIImage.init(named: "navigation_image"), style: .plain, target: self, action: #selector(onMenuButtonAction))
        self.navigationItem.leftBarButtonItem = button
        return button
    }
    
    var crossBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "cancelCrossIcon"), style: .plain, target: self, action: #selector(onCrossButtonAction))
        self.navigationItem.leftBarButtonItem = button
        return button
    }
    
    var threeDotBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "moreIcon"), style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = button
        return button
    }
    
    var videoCallBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "videoCallicon"), style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = button
        return button
    }
    
    var favBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "favIcon"), style: .plain, target: nil, action: nil)
        return button
    }
    
    var searchBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "searchIcon"), style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = button
        return button
    }
    
    var addBudEChatBarButton: UIBarButtonItem {
        let button = UIBarButtonItem (image: #imageLiteral(resourceName: "addBudEChatIcon"), style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = button
        return button
    }
    
    var crossButton: UIButton {
        let button = UIButton (frame: CGRect (x: 15.5 * scaleFactorX, y: 33.5 * scaleFactorX, width: 10, height: 10))
        button.setImage(#imageLiteral(resourceName: "cancelCrossIcon"), for: .normal)
        button.sizeToFit()
        button.frame = CGRect (x: 0, y: 0, width: (15.5 * scaleFactorX * 2) + button.frame.size.width, height: (33.5 * scaleFactorX * 2) + button.frame.size.height)
        button.addTarget(self, action: #selector(onCrossButtonAction), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }
    
    var backButton: UIButton {
        let button = UIButton (frame: CGRect (x: 15.5 * scaleFactorX, y: 33.5 * scaleFactorX, width: 10, height: 10))
        button.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        button.sizeToFit()
        button.frame = CGRect (x: 0, y: 0, width: (15.5 * scaleFactorX * 2) + button.frame.size.width, height: (33.5 * scaleFactorX * 2) + button.frame.size.height)
        button.addTarget(self, action: #selector(onBackButtonAction), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }
    
    func addMultipleBarButtons(types: BarButtonTypes, position: BarButtonPosition) {
        if types.contains(.searchButton) {
            
        }
    }
    
    @objc func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onMenuButtonAction() {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @objc func onBackButtonActionForReset() {
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is LoginViewController {
                _ = self.navigationController?.popToViewController(vc as! LoginViewController, animated: true)
                return
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onCrossButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func isFirstInHierarchy() -> Bool {
        guard self.navigationController != nil else {
            return true
        }
        return self.navigationController!.viewControllers.first == self
    }
    
    func showNavigationBar() {
        guard self.navigationController != nil else {
            return
        }
        self.navigationController? .setNavigationBarHidden(false, animated: true)
        
    }
    
    func hideNavigationBar() {
        guard self.navigationController != nil else {
            return
        }
        self.navigationController? .setNavigationBarHidden(true, animated: true)
    }
    
    func addMoreBarButtonItem(contents: [String], delegate: UIViewController) {
        let button = threeDotBarButton
        button.target = delegate
        button.tag = 0
        button.action = #selector(onMoreBarButtonAction(_:))
    }
    
    @objc func onMoreBarButtonAction(_ barButton: UIBarButtonItem) {
        
    }
}
