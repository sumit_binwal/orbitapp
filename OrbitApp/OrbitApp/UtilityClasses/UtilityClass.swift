//
//  UtilityClass.swift
//  MyanCareDoctor
//
//  Created by Santosh on 19/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MobileCoreServices
import Photos
import SafariServices
import NVActivityIndicatorView

/// usr default keys
///
/// - userInfoData: userInfoData description
/// - notificationStatus: notificationStatus description
/// - fcmDeviceToken: fcmDeviceToken description
/// - chatUnReadCount: chatUnReadCount description
/// - apnsDeviceToken: apnsDeviceToken description
private enum UserDefaultEnum : String {
    
    case userInfoData = "userInfoData"
    case notificationStatus = "notificationStatus"
    case fcmDeviceToken = "FCM_Token"
    case chatUnReadCount = "chat_un_read_count"
    case apnsDeviceToken = "APNS_Token"
}

class UtilityClass: NSObject {
    
    /// start loader
    class func startAnimating() {
        
        let size = CGSize(width: 50, height: 50)

        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData.init(size: size, message: "", messageFont: nil, messageSpacing: nil, type: NVActivityIndicatorType.ballClipRotateMultiple, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil), nil)
    }
    
    /// stop loader
    class func stopAnimating() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    /// start animating
    ///
    /// - Parameter onViewController: onViewController description
    class func startAnimating(onViewController: UIViewController) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData (), nil)
    }
    
    /// stop animating loader
    ///
    /// - Parameter onViewController: onViewController description
    class func stopAnimating(onViewController: UIViewController) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    //MARK:- Extract Number
    
    /// extract number from string
    ///
    /// - Parameter string: string description
    /// - Returns: return value description
    class func extractNumber(fromString string:String) -> String {
        
        let characterSet = CharacterSet.decimalDigits.inverted
        let stringArray = string.components(separatedBy: characterSet)
        let newString = stringArray.joined(separator: "")
        
        return newString
    }
    
    //MARK:-
    //MARK:- Open Safari ViewController as A WebView
    
    /// opern safari controller
    ///
    /// - Parameters:
    ///   - openLink: openLink description
    ///   - onViewController: onViewController description
    class func openSafariController(usingLink openLink:LinksEnum, onViewController: UIViewController?) -> Void {
        
        guard let visibleController = onViewController else {
            return
        }
        
        let safariController : SFSafariViewController = SFSafariViewController(url: URL(string: openLink.rawValue)!)
        safariController.delegate = visibleController as? SFSafariViewControllerDelegate
        visibleController.present(safariController, animated: true, completion: nil)
    }
    
    //MARK:- Open Safari browser
    
    /// open safari browser
    ///
    /// - Parameter urlLink: urlLink description
    class func openSafariBrowser (usingLink urlLink : LinksEnum) -> Void {
        
        let openUrl = URL(string: urlLink.rawValue)
        
        let application:UIApplication = UIApplication.shared
        
        if (application.canOpenURL(openUrl!)) {
            if #available(iOS 10.0, *) {
                application.open(openUrl!, options: [:], completionHandler: nil)
            }
            else {
                // Fallback on earlier versions
                print("other versions")
                application.openURL(openUrl!)
            }
        }
        else {
            print("can not open url")
        }
    }
    
    //MARK:- get timeStamp from current data
    
    /// get current time stump
    ///
    /// - Returns: return value description
    class func getCurrentTimeStamp () -> String {
        
        // Get the Unix timestamp
        let timestamp = Date().timeIntervalSince1970
        //print(timestamp)
        
        return String(Int(timestamp))
    }
    
    //MARK:- Change RootViewController
    
    /// change root view controller
    ///
    /// - Parameter newRootViewController: newRootViewController description
    class func changeRootViewController(with newRootViewController : UIViewController) -> Void {
        
        if let navigationCont = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navigationCont.popToRootViewController(animated: false)
        }
        
        APPDELEGATE.window?.rootViewController = newRootViewController
    }
    
    //MARK:- Action Sheet
    
    /// open action sheet
    ///
    /// - Parameters:
    ///   - title: title description
    ///   - message: message description
    ///   - onViewController: onViewController description
    ///   - buttonArray: buttonArray description
    ///   - dismissHandler: dismissHandler description
    static func showActionSheetWithTitle(title:String? = String.MyApp.AppName, message:String? = String.MyApp.defaultAlertMessage, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray {
            for item in buttonArray! {
                
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
  
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- get timeStamp from date
    
    /// get time stamp from date string
    ///
    /// - Parameter dateString: dateString description
    /// - Returns: return value description
    class func getTimeStampFromDate (dateString : String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }
    
    //MARK:- get timeStamp from time
    
    /// get time stamp from time string
    ///
    /// - Parameter dateString: dateString description
    /// - Returns: return value description
    class func getTimeStampFromTime (dateString : String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }

    //MARK:- Get Comma Seprated String From Array
    
    /// get comma seperated string from dictionary
    ///
    /// - Parameters:
    ///   - completeArray: completeArray description
    ///   - key: key description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArrayDict(completeArray:[[String:Any]], withKeyName key:String) -> String {
        
        var nameArr: [String] = []
        
        for name in completeArray {
            let nameStr = name[key] as! String
            nameArr.append(nameStr)
        }
       
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    /// get comma seperated string from array
    ///
    /// - Parameter arr: arr description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArray(completeArray arr : NSMutableArray) -> String {
        
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    /// get comma sepearted string from NSMutableArray
    ///
    /// - Parameter arr: arr description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArrayDoctorFilter(completeArray arr : NSMutableArray) -> String {
        
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
    class func convertDateFormater(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "mm/dd/yyyy"
        
        return  dateFormatter.string(from: date!)
    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
    class func getTimeFromDateString(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        
        return  dateFormatter.string(from: date!)
    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
    class func getDateFromDateString(_ date: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        
        return date!
    }
 
    /// function to perform date format
    ///
    /// - Parameters:
    ///   - givenDateStr: Date string
    ///   - currentDateFormat: current format
    ///   - resultingDateFormat: result format
    /// - Returns: Date string
    class func getDateStringFromStr(givenDateStr : String, andCurrentDateFormat currentDateFormat : String, andResultingDateFormat resultingDateFormat : String) -> String {
        
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = currentDateFormat
        
        if let date = dateformatter.date(from: givenDateStr) {
            dateformatter.dateFormat = resultingDateFormat
            let dateStr = dateformatter.string(from: date)
            
            return dateStr
        }
        else {
            dateformatter.dateFormat = resultingDateFormat
            let dateStr = dateformatter.string(from: Date())
            
            return dateStr
        }
    }
    
    //MARK: - Present Image PickerController
    
    /// show action sheet for image options
    ///
    /// - Parameter controller: controller description
    class func showImagePickerControllerForMedia(onController controller: UIViewController) {
        
        UIAlertController.showActionSheetWithTitle(title: "Choose Option", message: "Choose option for media", onViewController: controller, withButtonArray: ["Camera", "Photos"]) { [weak controller] (buttonIndex) in
            
            if buttonIndex == LONG_MAX {
                return
            }
            
            guard let `controller` = controller else {return}
            
            let imagePicker = UIImagePickerController ()
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.delegate = controller as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            
            if buttonIndex == 0 {
                if UIImagePickerController.isSourceTypeAvailable (UIImagePickerController.SourceType.camera) {
                    
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    imagePicker.allowsEditing = true
                    
                    controller.present(imagePicker, animated: true, completion: nil)
                }
            }
            
            if buttonIndex == 1 {
                if UIImagePickerController.isSourceTypeAvailable (UIImagePickerController.SourceType.photoLibrary) {
                    
                    imagePicker.sourceType = .savedPhotosAlbum
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    imagePicker.allowsEditing = true
                    
                    controller.present(imagePicker, animated: true, completion: nil)
                }
            }
        }
    }
    
    /// get image url
    ///
    /// - Parameters:
    ///   - imageURL: imageURL description
    ///   - image: image description
    /// - Returns: return value description
    class func getImageURL(imageURL: String, image:UIImage)-> NSURL {
  
        var documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        documentDirectory = documentDirectory! + "/"
    
        let localPath = documentDirectory?.appending(imageURL)
    
        let image = image
        let data = image.pngData()! as NSData
        data.write(toFile: localPath!, atomically: true)
    
        _ = NSData(contentsOfFile: localPath!)!
    
        let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
    
        return photoURL as NSURL
    }
    
    /// function to get Documents Directory Path
    ///
    /// - Returns: URL value
    class func getDocumentsDirectoryPath() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    /// function to get Documents Folder
    ///
    /// - Parameter folderName: string value
    /// - Returns: URL value
    class func getDocumentsFolder(withName folderName: String) -> URL? {
        
        let documentsPath = UtilityClass.getDocumentsDirectoryPath()
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path) {
            do {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                return nil
            }
        }
        
        return newDirectoryPath
    }
    
    /// function to add file in folder
    ///
    /// - Parameters:
    ///   - folder: folder name -> string value
    ///   - fileName: file name -> string value
    ///   - fileData: Data value
    /// - Returns: bool value
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool {
        
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    /// function to get file url from folder
    ///
    /// - Parameters:
    ///   - folder: folder name -> string value
    ///   - fileName: file name -> string value
    /// - Returns: URL value
    class func getFileURLFromFolder(_ folder: String, fileName: String) -> URL? {
        
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: newFilePath.path) {
            return newFilePath
        }
        
        return nil
    }
}


